locals {
  accounts = {
    for k in fileset("config", "account-*.yaml") :
    trimsuffix(k, ".yaml") => yamldecode(file("config/${k}"))
  }

  bsls = {
    for k in fileset("config", "bsl-*.yaml") :
    trimsuffix(k, ".yaml") => yamldecode(file("config/${k}"))
  }

  profiles = {
    for k in fileset("config", "profile-*.yaml") :
    trimsuffix(k, ".yaml") => yamldecode(file("config/${k}"))
  }

  appliances = {
    for k in fileset("config", "appliance-*.yaml") :
    trimsuffix(k, ".yaml") => yamldecode(file("config/${k}"))
  }

  clusters = {
    for k in fileset("config", "cluster-*.yaml") :
    trimsuffix(k, ".yaml") => yamldecode(file("config/${k}"))
  }
}

module "Spectro" {
  source = "github.com/spectrocloud/terraform-spectrocloud-modules"
  #source  = "spectrocloud/modules/spectrocloud"
  #version = "0.0.7"

  accounts   = local.accounts
  bsls       = local.bsls
  profiles   = local.profiles
  appliances = local.appliances
}

module "SpectroClusters" {
  depends_on = [module.Spectro]
  source     = "github.com/spectrocloud/terraform-spectrocloud-modules"
  #source  = "spectrocloud/modules/spectrocloud"
  #version = "0.0.7"

  clusters = local.clusters
}
